//
//  main.m
//  LCBO Companion
//
//  Created by Soner Yuksel on 2017-05-01.
//  Copyright © 2017 com.plasticmobile.lcbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
