//
//  AppDelegate.h
//  LCBOCompanion
//
//  Created by Brandon Anthony on 2017-05-02.
//  Copyright © 2017 Plastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

