//
//  main.m
//  LCBOCompanion
//
//  Created by Brandon Anthony on 2017-05-02.
//  Copyright © 2017 Plastic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
